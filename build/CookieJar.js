const   {URL}           =   require("url"),
        CookieJar       =   function() {
            this._cookies = {};
        };

CookieJar.prototype = {
    get cookies() {
        return this._cookies;
    },
    get_cookie(url,parsed,encode) {
        let domain, response;
        parsed = typeof(parsed) === "boolean" ? parsed : true;
        if (typeof(encode) !== "boolean") encode = parsed ? false : true;
        if (url) domain = new URL(/^https?:\/\//gi.test(url) ? url : `https://${url}`).host;
        if (!domain||!this.cookies.hasOwnProperty(domain)) return undefined;
        response = this.cookies[domain];
        if (response.hasOwnProperty("expires") && new Date() > new Date(response.expires)) {
            this.delete_cookie(domain);
            return undefined;
        }else {
            if (encode) for (let key of Object.keys(response)) response[key] = encodeURIComponent(response[key]);
            return parsed ? response : this.stringify(response);
        }
    },
    get_cookieKey(url,key,encode) {
        let domain;
        if (typeof(encode) !== "boolean") encode = false;
        if (url) domain = new URL(/^https?:\/\//gi.test(url) ? url : `https://${url}`).host;
        if (
            !domain||
            !key||
            !this.cookies.hasOwnProperty(domain)||
            (
                this.get_cookie(domain)&&
                !this.get_cookie(domain).hasOwnProperty(key)
            )
        ) return undefined;
        return encode ? encodeURIComponent(this.cookies[domain][key]) : this.cookies[domain][key];
    },
    parse(string,decode) {
        if (typeof(decode) !== "boolean") decode = true;
        let response = {},
            tmp = string.split(";").map(e=>e.trim().split("="));
        tmp.forEach(entry=>response[entry[0]] = decode ? decodeURIComponent(entry[1]) : entry[1]);
        return response;
    },
    stringify(cookies) {
        return Object.entries(cookies)
            .map(e=>e[1] ? e.join("=") : e[0])
            .join("; ");
    },
    add_cookies(cookies,url) {
        let domain;
        if (url) domain = new URL(/^https?:\/\//gi.test(url) ? url : `https://${url}`).host;
        if (!domain||!cookies) throw new Error("Argument(s) manquant(s)");
        if (!this.get_cookie(domain)) this.cookies[domain] = {};
        if (typeof(cookies) === "string") cookies = this.parse(cookies);
        this.cookies[domain] = Object.assign(this.cookies[domain],cookies);
    },
    delete_cookie(url,key) {
        let domain;
        if (url) domain = new URL(/^https?:\/\//gi.test(url) ? url : `https://${url}`).host;
        if (!domain) throw new Error("Aucun domaine spécifié");
        if (
            !this.cookies.hasOwnProperty(domain)||
            (
                this.get_cookie(domain)&&
                !this.get_cookie(domain).hasOwnProperty(key)
            )
        ) throw new Error(`Aucun cookie exitant pour le domaine "${domain}" et la clé "${key}"`);

        if (typeof(key) !== "undefined") delete this.cookies[domain][key];
        else delete this.cookies[domain];
    }
};

module.exports = CookieJar;
